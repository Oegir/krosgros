<?php

/**
 * @author oegir alexey.webmail@gmail.com
 * Поиск палиндрома с помощью алгоритма Манкера
 */

// Точка входа в программу
PageRenderer::executeController();

/**
 * Реализация алгоритма на PHP
 *
 * @link https://ru.wikipedia.org/wiki/%D0%9F%D0%BE%D0%B8%D1%81%D0%BA_%D0%B4%D0%BB%D0%B8%D0%BD%D0%BD%D0%B5%D0%B9%D1%88%D0%B5%D0%B9_%D0%BF%D0%BE%D0%B4%D1%81%D1%82%D1%80%D0%BE%D0%BA%D0%B8-%D0%BF%D0%B0%D0%BB%D0%B8%D0%BD%D0%B4%D1%80%D0%BE%D0%BC%D0%B0
 */
class ManachersAlgorithm
{

    /**
     *
     * @param string $s
     *            Строка, в которой ищется палиндром
     * @return string Самый длинный, найденный палиндром
     */
    public static function findLongestPalindrome($s)
    {
        if (empty($s))
            return '';
        
        $s2 = self::addBoundaries(preg_split('//u', $s, null, PREG_SPLIT_NO_EMPTY));
        $p = array_fill(0, count($s2), '');
        $c = 0;
        $r = 0;
        $m = 0;
        $n = 0;
        
        for ($i = 1; $i < count($s2); $i ++) {
            
            if ($i > $r) {
                $p[$i] = 0;
                $m = $i - 1;
                $n = $i + 1;
            } else {
                $i2 = $c * 2 - $i;
                
                if ($p[$i2] < ($r - $i)) {
                    $p[$i] = $p[$i2];
                    $m = - 1;
                } else {
                    $p[$i] = $r - $i;
                    $n = $r + 1;
                    $m = $i * 2 - $n;
                }
            }
            while ($m >= 0 && $n < count($s2) && $s2[$m] == $s2[$n]) {
                $p[$i] ++;
                $m --;
                $n ++;
            }
            if (($i + $p[$i]) > $r) {
                $c = $i;
                $r = $i + $p[$i];
            }
        }
        $len = 0;
        $c = 0;
        for ($i = 1; $i < count($s2); $i ++) {
            
            if ($len < $p[$i]) {
                $len = $p[$i];
                $c = $i;
            }
        }
        $ss = array_slice($s2, $c - $len, $c + $len + 1);
        return implode(self::removeBoundaries($ss));
    }

    private static function addBoundaries(array $cs)
    {
        if (empty($cs) || count($cs) == 0)
            return array(
                '|',
                '|'
            );
        
        $cs2 = array_fill(0, count($cs) * 2 + 1, '');
        for ($i = 0; $i < (count($cs2) - 1); $i = $i + 2) {
            $cs2[$i] = '|';
            $cs2[$i + 1] = $cs[$i / 2];
        }
        $cs2[count($cs2) - 1] = '|';
        
        return $cs2;
    }

    private static function removeBoundaries(array $cs)
    {
        if (empty($cs) || count($cs) < 3)
            return array(
                ''
            );
        
        $cs2 = array_fill(0, (count($cs) - 1) / 2, '');
        for ($i = 0; $i < count($cs2); $i ++) {
            $cs2[$i] = $cs[$i * 2 + 1];
        }
        return $cs2;
    }
}

/**
 * Класс, отвечающий за взаимодействие с пользователем
 * Для уменьшения объема кода постараемся реализовать MVC парадигму единственным классом,
 * при этом Контроллер, Модель и Вювер будут представлены его методами.
 * Систему шаблонов реализуем с помощью отдельных функций, что бы подчеркнуть их независимость и заменяемость
 *
 * @author oegir
 */
class PageRenderer
{

    /**
     * Выясняет тербуемое действие и запускает его исполнение
     */
    public static function executeController()
    {
        // Получим данные запроса
        $action = ! empty($_GET['action']) ? self::_decode($_GET['action']) : 'default';
        $template = ! empty($_GET['tpl']) ? self::_decode($_GET['tpl']) : 'default';
        // Используем модель
        $model = self::getModel();
        $params = array();
        
        switch ($action) {
            case 'find_palindrom':
                $source = ! empty($_GET['source_string']) ? self::_decode($_GET['source_string']) : '';
                $params['source_value'] = $source;
                $closure = $model->findPalindrom;
                try {
                    $closure($model, $source);
                }
                catch (Exception $e) {
                    $params['message'] = $e->getMessage();
                }
                break;
            case 'default':
            default:
                // Особых действий не требуется
                break;
        }
        // Задействуем вьювер для отображения
        self::viewProcessTemplate($model, $params, $template);
    }

    /**
     * Вся работа происходит в UTF-8, преобразуем к ней входные данные используя готовую функцию из OpenSource проекта Joomla
     *
     * Try to convert to plaintext
     *
     * @param string $source
     *            The source string.
     *            
     * @return string Plaintext string
     *        
     * @since 11.1
     * @link https://github.com/joomla/joomla-cms/blob/3.4.x/libraries/joomla/filter/input.php#LC956
     */
    private function _decode($source)
    {
        static $ttr;
        if (! is_array($ttr)) {
            // Entity decode
            $trans_tbl = get_html_translation_table(HTML_ENTITIES, ENT_COMPAT, 'ISO-8859-1');
            foreach ($trans_tbl as $k => $v) {
                $ttr[$v] = utf8_encode($k);
            }
        }
        $source = strtr($source, $ttr);
        // Convert decimal
        $source = preg_replace_callback('/&#(\d+);/m', function ($m) {
            return utf8_encode(chr($m[1]));
        }, $source);
        // Convert hex
        $source = preg_replace_callback('/&#x([a-f0-9]+);/mi', function ($m) {
            return utf8_encode(chr('0x' . $m[1]));
        }, $source);
        return $source;
    }

    /**
     * Выполняет поиск палиндрома в строке
     *
     * @param string $param
     *            Строка для обработки
     * @return StdClass Модель с резултатом обработки
     */
    private static function getModel($param = null)
    {
        // Используем stdClass для представления модели.
        // Формально мы не вводили отдельный класс модели, а все еще работаем в рамках одного метода
        $model = new stdClass();
        // Используем композицию для задействования алгоритма Манкера
        $model->palindromFinder = ManachersAlgorithm;
        $model->data = Null;
        // У модели должны быть методы для изменения ее состояния, симулируем это лямбда-функцией
        $model->findPalindrom = function ($model, $s) {
            $err_msg = '';
            
            if (strpos($s, '|') !== false) {
                $err_msg .= 'Символ "|" используется в качестве служебного при поиске палиндрома, его наличие во входных данных не допускается.' . PHP_EOL;
            }
            if (! empty($err_msg)) {
                throw new Exception($err_msg);
            }
            
            $process_string = mb_strtolower(preg_replace('/\W/u', '', $s), 'UTF-8');
            $finder = $model->palindromFinder;
            $palindrom = $finder::findLongestPalindrome($process_string);
            $model->data = mb_strlen($palindrom, 'UTF-8') > 1 ? $palindrom : 'Lorem Ipsum';
        };
        return $model;
    }

    /**
     * Получает данные из модели и отображает требуемый шаблон
     *
     * @param stdClass $model
     *            Объект модели
     * @param array $params
     *            Дополнительные параметры, которые могут быть переданы в шаблон
     * @param string $template
     *            Имя отображаемого шаблона
     */
    private static function viewProcessTemplate(stdClass $model, array $params = array(), $template = 'default')
    {
        $view_params['result'] = $model->data;
        $view_params['form_action'] = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
        $tmpl_params = array_merge($params, $view_params);
        
        if (! empty($template) && $template != 'default') {
            // Если передано имя шаблона используем его
            $tmpl_name = $template;
        } elseif (! is_null($model->data)) {
            // Если есть данные используем шаблон для отображения
            $tmpl_name = 'result';
        }
        $tmpl_to_render = function_exists('template_' . $tmpl_name) ? 'template_' . $tmpl_name : 'template_default';
        
        echo <<< EOF
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

EOF;
        $tmpl_to_render($tmpl_params);
        echo <<< EOF
</body>
</html>
EOF;
    }
}

/**
 * Дефолтный шаблон
 *
 * @param array $params
 *            Параметры шаблона
 */
function template_default($params = array())
{
    extract($params);
    ?>
<form action="<?php echo $form_action ?>" method="get">
	<div style="float: left;">
		<label><span>Введите текст: </span> <input type="text"
			name="source_string" size="40" maxlength="200" value=""> </label>
	</div>
	<div>
		<input type="submit" value="Найти палиндром">
	</div>
	<input type="hidden" name="action" value="find_palindrom">
	<input type="hidden" name="tpl" value="result">
</form>
<?php
}

/**
 * Шаблон вывода результатов поиска
 *
 * @param array $params
 *            Параметры шаблона
 */
function template_result($params = array())
{
    extract($params);
    ?>
<form action="<?php echo $form_action ?>" method="get">
	<div style="float: left;">
		<label><span>Введите текст: </span> <input type="text"
			name="source_string" size="40" maxlength="200"
			value="<?php echo $source_value; ?>"> </label>
	</div>
	<div>
		<input type="submit" value="Найти палиндром">
	</div>
	<?php if (!empty($result)): ?>
	<div>
		<span>Найден палиндром: </span><span><?php echo $result; ?></span>
	</div>
	<?php endif; ?>
	<?php if (!empty($message)): ?>
	<div>
		<span><?php echo $message; ?></span>
	</div>
	<?php endif; ?>
	<input type="hidden" name="action" value="find_palindrom">
	<input type="hidden" name="tpl" value="result">
</form>
<?php
}
