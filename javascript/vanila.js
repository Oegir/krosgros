/**
 * Раскрашивание текста средствами чистого JavaScript
 */

/**
 * Конструктор объекта, раскрашивающего весь текст в один цвет
 * @param selector String SCC-селектор, выбирающий элемент для раскрашивания
 */
function TextColoring(selector) {
	this.textElement = document.querySelector(selector);

	if (this.textElement instanceof Element) {
		this.text = this.textElement.textContent;
	}
}


// Создает inline-элемент, с установленным свойством цвета
// @param word String Текст в элементе
// @param color String Цвет элемента
// @returns Element
TextColoring.prototype.createColored = function (word, color) {
	var span = document.createElement('span');
	span.innerHTML = word;
	span.style.color = color;
	return span;
}

// Окрашивает весь текст в один цвет
// @param color String Значение цвета
TextColoring.prototype.colorText = function (color) {
	this.textElement.innerHTML = '';
	this.textElement.appendChild(this.createColored(this.text, color));
}

/**
 * Конструктор объекта, раскрашивающего текст в один или несколько цветов
 * @param selector String SCC-селектор, выбирающий элемент для раскрашивания
 */
function WordsColoring(selector) {
	TextColoring.call(this, selector);
}
WordsColoring.prototype = Object.create(TextColoring.prototype);
WordsColoring.prototype.constructor = WordsColoring;

// Раскрашивает слова текста в один или несколько цветов
// @param colors Mixed Строка или массив строк со значениями цветов
WordsColoring.prototype.colorText = function(colors) {
	
	if (typeof colors == 'string') {
		TextColoring.prototype.colorText.call(this, colors);
	} else {
		this.textElement.innerHTML = '';
		var words = this.text.split(' ');
		
		for (var i = 0; i < words.length; i++) {
			color = colors[i % colors.length];
			this.textElement.appendChild(this.createColored(words[i], color));
			this.textElement.appendChild(document.createTextNode(' '));
		}
	}
};

function run() {
//	var textColoring = new TextColoring('table.t td:nth-child(2)');
//	textColoring.colorText('#f00');
	var wordsColoring = new WordsColoring('table.t td:nth-child(2)');
//	wordsColoring.colorText('#f00');
	wordsColoring.colorText([ '#f00', '#0f0', '#00f' ]);
}