SELECT 
    c1.*
FROM
    category AS c1
        LEFT JOIN
    category AS c2 ON c1.id = c2.parent_category_id
WHERE
    c2.id IS NULL;