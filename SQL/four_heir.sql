SELECT 
    c1.name AS lev1,
    c2.name AS lev2,
    c3.name AS lev3,
    c4.name AS lev4
FROM
    category AS c1
        LEFT JOIN
    category AS c2 ON c2.parent_category_id = c1.id
        LEFT JOIN
    category AS c3 ON c3.parent_category_id = c2.id
        LEFT JOIN
    category AS c4 ON c4.parent_category_id = c3.id
		LEFT JOIN
    category AS c5 ON c5.parent_category_id = c4.id
WHERE
    c5.name IS NULL AND c1.parent_category_id IS NULL;